<?php
if (!empty($shipping_methods)) {
    $cnt_shipping_methods = count($shipping_methods);
    ?>
    <?php foreach ($shipping_methods as $index => $method) : ?>
        <label class="btn btn-shipping-method">
            <?php $value = "{$method->method_id}:{$method->instance_id}"; ?>
            <input
            <?php
            if (($cnt_shipping_methods == 1) || ($checked_value == $value)) {
                echo 'checked="checked"';
            }
            ?>
                type="radio" name="shipping_method" data-index="<?php echo $index; ?>" id="shipping_method_<?php echo "{$method->method_id}{$method->instance_id}"; ?>" value="<?php echo "{$method->method_id}:{$method->instance_id}"; ?>" class="shipping_method shipping_method_radio" />
            <i class="fa fa-circle-o fa-2x"></i>
            <i class="fa fa-dot-circle-o fa-2x"></i>
            <span>  <?php echo esc_attr($method->method_title); ?></span>
        </label>
    <?php endforeach; ?>
<?php } ?>