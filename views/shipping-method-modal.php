<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>

<?php if (isset($atts['show_as']) && !empty($atts['show_as'])) { ?>
    <a class="set-shipping
    <?php
    if ($atts['show_as'] == 'button') {
        echo "btn btn-primary";
    }
    echo ' ' . $atts['class'];
    ?>" href="javascript:void(0);"><?php echo $atts['label']; ?></a>
   <?php } ?>

<?php if ($atts['show_on_page_load'] == 'true') { ?>
    <div style="height: 0; width: 0; display: none;" attr-show_on_page_load="1" id="show_on_page_load"></div>
<?php } ?>

<?php if (!empty($atts['country']) && ($atts['hide_country_list'] == 'true')) { ?>
    <div style="height: 0; width: 0; display: none;" attr-hide_country_list="1" id="hide_country_list"></div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="shipping-methods" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $atts['label']; ?></h4>
            </div>

            <div class="modal-body">

                <div id="shipping_method_error" class="alert alert-danger clearfix">
                    Please select shipping method.
                </div>

                <div id="country_dropdown"><?php echo $country_dropdown; ?></div>

                <div class="btn-group btn-group" data-toggle="buttons" id="global_shipping_methods">
                    <?php echo $default_shipping_methods_radio; ?>
                </div>

                <div class="btn-group btn-group" id="wrapper_global_shipping_methods_post_code">
                    <input type="text" id="global_shipping_methods_post_code" placeholder="Postcode" value="<?php echo $post_code; ?>">
                    <input type="hidden" id="global_selected_shipping_method_label">
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success set-global-shipping-method">SAVE ME</button>
            </div>


        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
