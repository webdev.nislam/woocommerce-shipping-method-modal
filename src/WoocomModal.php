<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WoocomModal
 *
 * @author NIslam
 */
class WoocomModal {

    private $initiated = false;

    public function __construct() {

        add_shortcode('woocommerce_shipping_modal', array($this, 'shipping_modal'));

        // called just before the woocommerce template functions are included
        add_action('init', array($this, 'register_session'), 1);
        add_action('init', array($this, 'init'), 20);

        // called only after woocommerce has finished loading
        add_action('woocommerce_init', array($this, 'woocommerce_loaded'));

        // called after all plugins have loaded
        add_action('plugins_loaded', array($this, 'plugins_loaded'));

        //Load all assets
        add_action('wp_enqueue_scripts', array($this, 'load_asstes'));
//        add_action('woocommerce_review_order_before_shipping', array($this, 'woocommerce_before_checkout_update_order_review'), 6);
        add_action('woocommerce_after_shipping_rate', array($this, 'woocommerce_before_checkout_update_order_review'), 6);
//        add_action('woocommerce_checkout_update_order_review', array($this, 'woocommerce_after_checkout_update_order_review'), 6);
        // indicates we are running the admin
        if (is_admin()) {
            // ...
        }

        // indicates we are being served over ssl
        if (is_ssl()) {
            // ...
        }

        add_action('wp_ajax_set_global_shipping_method', array($this, 'set_global_shipping_method'));
        add_action('wp_ajax_nopriv_set_global_shipping_method', array($this, 'set_global_shipping_method'));

        add_action('wp_ajax_get_shipping_method_by_zone', array($this, 'get_shipping_method_by_zone'));
        add_action('wp_ajax_nopriv_get_shipping_method_by_zone', array($this, 'get_shipping_method_by_zone'));
    }

    /**
     * Take care of anything that needs woocommerce to be loaded.
     * For instance, if you need access to the $woocommerce global
     */
    public function woocommerce_loaded() {
        return;
    }

    public function load_asstes() {
        if (!is_admin()) {
            wp_register_style('bootstarp-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7');
            wp_enqueue_style('bootstarp-css');

            wp_register_style('style-css', WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_URL . 'assets/css/style.css', array('bootstarp-css'), WOOCOMMERCE_SHIPPING_METHOD_MODAL_VERSION);
            wp_enqueue_style('style-css');

            wp_register_script('bootstarp-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '4.0.0');
            wp_enqueue_script('bootstarp-js');

            wp_register_style('select2-css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array('bootstarp-css'), '1.0.0');
            wp_enqueue_style('select2-css');

            wp_register_script('select2-js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array('bootstarp-js'), '1.0.0');
            wp_enqueue_script('select2-js');

            wp_register_script('cookie-js', '//cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js', array('bootstarp-js'), WOOCOMMERCE_SHIPPING_METHOD_MODAL_VERSION);
            wp_enqueue_script('cookie-js');

            wp_register_script('shipping-modal-js', WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_URL . 'assets/js/shipping-modal.js', array('select2-js'), WOOCOMMERCE_SHIPPING_METHOD_MODAL_VERSION);
            wp_enqueue_script('shipping-modal-js');

            $ajax_params = array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'ajax_nonce' => wp_create_nonce('set_shipping_method_nonce'),
                'ajax_get_shipping_method_nonce' => wp_create_nonce('get_shipping_method_nonce'),
            );
            wp_localize_script('shipping-modal-js', 'ajax_shipping_method_object', $ajax_params);
        }
    }

    /**
     * Take care of anything that needs all plugins to be loaded
     */
    public function plugins_loaded() {
        return;
    }

    public function init() {
        if (!$this->initiated) {
            $this->initHooks();
        }
    }

    public function shipping_modal($attr) {

        $this->isWooCommercerActive();

        $atts = shortcode_atts(
                array(
            'show_as' => '',
            'label' => 'Set Shipping Method',
            'class' => '',
            'show_on_page_load' => 'false',
            'country' => '',
            'hide_country_list' => 'false'
                ), $attr, 'woocommerce_shipping_modal');

        $choosen_shipping_method = json_decode(stripslashes($this->getChoosenShippingMethod()));
        $post_code = (isset($_COOKIE['shipping_method_selected_postcode']) && !empty($_COOKIE['shipping_method_selected_postcode']) ) ? $_COOKIE['shipping_method_selected_postcode'] : '';

        $default_checkout_country = $atts['country'];
        if (empty($default_checkout_country)) {
            $default_checkout_country = $this->getDefaultCheckoutCountry();
        }

        $country_dropdown = $this->getWcCountriesDropdown($default_checkout_country);

        $shipping_methods = $this->getShippingMethodsByZoneCode($default_checkout_country);
        $default_shipping_methods_radio = $this->getShippingMethodsView($shipping_methods, $choosen_shipping_method[0]);

        ob_start();
        require_once(WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_DIR . 'views/shipping-method-modal.php');
        $modal = ob_get_contents();
        ob_end_clean();
        return $modal;
    }

    public function woocommerce_before_checkout_update_order_review() {
        global $woocommerce;

        $chosen_shipping_methods_from_cookie = json_decode(stripslashes($_COOKIE['chosen_shipping_methods']));
        $chosen_shipping_methods_from_session = WC()->session->get('chosen_shipping_methods');

        if ($chosen_shipping_methods_from_cookie[0] != $chosen_shipping_methods_from_session[0]) {
            $chosen_shipping_methods = $chosen_shipping_methods_from_session;
        }

        setcookie('chosen_shipping_methods', json_encode($chosen_shipping_methods_from_session), 0, '/');
        WC()->session->set('chosen_shipping_methods', $chosen_shipping_methods_from_session);
    }

    public function woocommerce_after_checkout_update_order_review() {
        global $woocommerce;

        $chosen_shipping_methods_from_cookie = json_decode(stripslashes($_COOKIE['chosen_shipping_methods']));

        if(is_null($chosen_shipping_methods_from_cookie)) {
            $shipping_methods = $this->getShippingMethodsByZoneCode('GB');
//
//            var_dump($shipping_methods);
//            exit;
        }

        $chosen_shipping_methods_from_session = WC()->session->get('chosen_shipping_methods');

        if ($chosen_shipping_methods_from_cookie[0] != $chosen_shipping_methods_from_session[0]) {
            $chosen_shipping_methods = $chosen_shipping_methods_from_session;
        }

        setcookie('chosen_shipping_methods', json_encode($chosen_shipping_methods_from_session), 0, '/');
        WC()->session->set('chosen_shipping_methods', $chosen_shipping_methods_from_session);
    }

    public function get_shipping_method_by_zone() {
        check_ajax_referer('get_shipping_method_nonce', 'security');

        $zone_code = wc_clean($_POST['zone_code']);
        $shipping_methods = $this->getShippingMethodsByZoneCode($zone_code);
        $choosen_shipping_method = json_decode(stripslashes($this->getChoosenShippingMethod()));

        $modal = $this->getShippingMethodsView($shipping_methods, $choosen_shipping_method[0]);

        $message['success'] = true;
        $message['html'] = $modal;
        $json_msg = wp_json_encode($message);
        wp_die($json_msg);
    }

    public function set_global_shipping_method() {
        global $woocommerce;
        check_ajax_referer('set_shipping_method_nonce', 'security');
        wc_maybe_define_constant('WOOCOMMERCE_CART', true);
        $postcodes = $woocommerce->customer->get_shipping_postcode();

        $is_valid_postcode = $this->validatePostCode(trim($_POST['shipping_method_selected_postcode']), trim($_POST['shipping_method_selected_country']));

        if (($_POST['shipping_method_label'] == 'Delivery') && !$is_valid_postcode) {
            $message['success'] = false;
            $message['msg'] = 'Invalid Postcode.';
            $json_msg = wp_json_encode($message);
            wp_die($json_msg);
        }

        if ($is_valid_postcode) {
            setcookie('shipping_method_selected_postcode', $_POST['shipping_method_selected_postcode'], 0, '/');
        }

        $chosen_shipping_methods = WC()->session->get('chosen_shipping_methods');
        $submitted_shipping_method = wc_clean($_POST['shipping_method']);

        if (isset($_POST['shipping_method']) && is_array($_POST['shipping_method'])) {
            foreach ($_POST['shipping_method'] as $i => $value) {
                $chosen_shipping_methods[$i] = wc_clean($value);
            }
        } else {
            $chosen_shipping_methods[0] = $submitted_shipping_method;
        }

        setcookie('chosen_shipping_methods', json_encode($chosen_shipping_methods), 0, '/');

        WC()->session->set('chosen_shipping_methods', $chosen_shipping_methods);
        $this->setSession('chosen_shipping_method', $submitted_shipping_method);

        $this->setDefaultCheckoutCountry(wc_clean($_POST['zone_code']));

        $message['success'] = true;
        $message['msg'] = 'shipping_method_updated';
        $json_msg = wp_json_encode($message);
        wp_die($json_msg);
    }

    public function plugin_activation() {

        if (version_compare($GLOBALS['wp_version'], WOOCOMMERCE_SHIPPING_METHOD_MODAL_WP_VERSION, '<')) {
            load_plugin_textdomain('woocommerce-shipping-modal');
            $message = '<strong>' . sprintf(esc_html__('Woocommerce Shipping Modal %s requires WordPress %s or higher.', 'woocommerce-shipping-modal'), WOOCOMMERCE_SHIPPING_METHOD_MODAL_VERSION, WOOCOMMERCE_SHIPPING_METHOD_MODAL_WP_VERSION) . '</strong> ' . sprintf(__('Please <a href="%1$s">upgrade WordPress</a> to a current version', 'woocommerce-shipping-modal'), 'https://codex.wordpress.org/Upgrading_WordPress');
            $this->bailOnActivation($message);
        }
        $this->isWooCommercerActive();
    }

    /**
     * Check if WooCommerce is active
     * */
    public function isWooCommercerActive() {

        if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            load_plugin_textdomain('woocommerce-shipping-modal');
            $message = '<strong>Please install woo commerce first</strong>';
            $this->bailOnActivation($message);
        }
    }

    public function plugin_deactivation() {
        return $this->deactivate_key($this->get_consumer_key());
    }

    public function deactivate_key($key) {
        return true;
    }

    public function get_consumer_key($key_api_consumer_key = 'woocommerce-shipping-modal') {
        return get_option($key_api_consumer_key);
    }

    public function register_Session() {

        // Set SSL level
        $https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);

        // Set the domain to default to the current domain.
        $domain = isset($_SERVER['SERVER_NAME']);

        // Set session cookie options
        session_set_cookie_params(3600, '/', $domain, $https, true);
        session_start();

        // Make sure the session hasn't expired, and destroy it if it has
        if ($this->validateSession()) {
            // Check to see if the session is new or a hijacking attempt
            if (!$this->preventHijacking()) {
                // Reset session data and regenerate id
                $_SESSION = array();
                $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                $this->regenerateSession();

                // Give a 5% chance of the session id changing on any request
            }
        } else {
            $_SESSION = array();
            session_destroy();
            session_start();
        }
    }

    private function validatePostCode($postCode, $country) {
        global $wpdb;

        $active_zone = $wpdb->get_row("SELECT zone_id FROM {$wpdb->prefix}woocommerce_shipping_zone_locations WHERE location_code = '{$country}' AND location_type = 'country'", OBJECT);

        if (empty($active_zone)) {
            return false;
        }

        $matchable_post_code_ar = explode(' ', $postCode);
        $matchable_post_code = $matchable_post_code_ar[0];

        $post_code = $wpdb->get_row("SELECT location_id FROM {$wpdb->prefix}woocommerce_shipping_zone_locations WHERE location_code LIKE '%{$matchable_post_code}%' AND zone_id = '{$active_zone->zone_id}' AND location_type = 'postcode'", OBJECT);

        if (empty($post_code)) {
            return false;
        }

        return true;
    }

    private function preventHijacking() {
        if (!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent']))
            return false;

        if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
            return false;

        if ($_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
            return false;

        return true;
    }

    private function validateSession() {
        if (isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
            return false;

        return true;
    }

    private function regenerateSession() {

        // Set current session to expire in 10 seconds
        $_SESSION['EXPIRES'] = time() + 3600;

        // Create new session without destroying the old one
        session_regenerate_id(false);

        // Grab current session ID and close both sessions to allow other scripts to use them
        $newSession = session_id();
        session_write_close();

        // Set session ID to the new one, and start it back up again
        session_id($newSession);
        session_start();

        // Now we unset the obsolete and expiration values for the session we want to keep
        unset($_SESSION['EXPIRES']);
    }

    private function getShippingMethodsView($shipping_methods, $checked_value = '') {
        ob_start();
        require_once(WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_DIR . 'views/partials/_shipping-methods.php');
        $modal = ob_get_contents();
        ob_end_clean();
        return $modal;
    }

    private function getWcCountriesDropdown($default_checkout_country = '', $b_print = false) {
        global $woocommerce;
        $countries_obj = new WC_Countries();
        $countries = $countries_obj->__get('countries');

        $country_filed_params = array(
            'type' => 'select',
            'class' => array('select2-shipping-zones'),
            'id' => 'select2-shipping-zones',
            'label' => __('Set Destination'),
            'placeholder' => __('Set Destination'),
            'options' => $countries,
        );

        ob_start();
        '<div id="my_custom_countries_field"><h2>' . __('Countries') . '</h2>';
        woocommerce_form_field('shipping_zone', $country_filed_params, $default_checkout_country);
        '</div>';
        $country_dropdown = ob_get_contents();
        ob_end_clean();

        if ($b_print) {
            echo $country_dropdown;
        } else {
            return $country_dropdown;
        }
    }

    private function getDefaultShippingMethods() {
        global $wpdb;

        $active_methods = $wpdb->get_results("SELECT instance_id, method_id FROM {$wpdb->prefix}woocommerce_shipping_zone_methods WHERE zone_id = 0 AND is_enabled = 1", OBJECT);
        $shipping_methods = $this->getShippingMethodLabels($active_methods);
        return $shipping_methods;
    }

    private function getShippingMethodsByZoneCode($code) {
        global $wpdb;

        $active_zone = $wpdb->get_row("SELECT zone_id FROM {$wpdb->prefix}woocommerce_shipping_zone_locations WHERE location_code = '{$code}' AND location_type = 'country'", OBJECT);
        if (empty($active_zone)) {
            $active_methods = $this->getDefaultShippingMethods();
        } else {
            $active_methods = $wpdb->get_results("SELECT instance_id, method_id FROM {$wpdb->prefix}woocommerce_shipping_zone_methods WHERE zone_id = {$active_zone->zone_id} AND is_enabled = 1", OBJECT);
            if (empty($active_methods)) {
                $active_methods = $this->getDefaultShippingMethods();
            }
        }

        $shipping_methods = $this->getShippingMethodLabels($active_methods);
        return $shipping_methods;
    }

    private function getShippingMethodLabels($methods) {

        foreach ($methods as $index => $method) {
            $opt_key = "woocommerce_{$method->method_id}_{$method->instance_id}_settings";
            $option = get_option($opt_key);
            if (!$option) {
                $methods[$index]->method_title = ucwords(str_replace('_', ' ', $method->method_id));
            } else {
                $methods[$index]->method_title = $option['title'];
            }
        }
        return $methods;
    }

    private function setDefaultCheckoutCountry($selected_zone) {
        global $woocommerce;

        WC()->customer->set_props(array(
            'shipping_country' => !is_null($selected_zone) ? wp_unslash($selected_zone) : null,
        ));

        WC()->customer->set_props(array(
            'billing_country' => !is_null($selected_zone) ? wp_unslash($selected_zone) : null,
        ));

        $this->setSession('chosen_shipping_country', $selected_zone);
        return true;
    }

    private function getChoosenShippingMethod() {
        global $woocommerce;
        $chosen_shipping_method = $this->getSession('chosen_shipping_methods');
        return $chosen_shipping_method;
    }

    private function getDefaultCheckoutCountry() {
        return $this->getSession('chosen_shipping_country');
    }

    private function initHooks() {
        $this->initiated = true;
    }

    private function setSession($key, $value) {
        $_SESSION[$key] = $value;
    }

    private function getSession($key) {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : isset($_COOKIE[$key]) ? $_COOKIE[$key] : null;
    }

    private function bailOnActivation($message, $deactivate = true) {
        ?>
        <!doctype html>
        <html>
            <head>
                <meta charset="<?php bloginfo('charset'); ?>">
                <style>
                    * {
                        text-align: center;
                        margin: 0;
                        padding: 0;
                        font-family: "Lucida Grande",Verdana,Arial,"Bitstream Vera Sans",sans-serif;
                    }
                    p {
                        margin-top: 1em;
                        font-size: 18px;
                    }
                </style>
            <body>
                <p><?php echo esc_html($message); ?></p>
            </body>
        </html>
        <?php
        if ($deactivate) {
            $plugins = get_option('active_plugins');
            $current_plugin = plugin_basename(WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_DIR . 'woocommerce-shipping-modal.php');
            $update = false;
            foreach ($plugins as $i => $plugin) {
                if ($plugin === $current_plugin) {
                    $plugins[$i] = false;
                    $update = true;
                }
            }

            if ($update) {
                update_option('active_plugins', array_filter($plugins));
            }
        }
        exit;
    }

}
