<?php

/**
  Plugin Name: Woocommerce Shipping Method Modal
  Plugin URI: https://wordpress.org/
  Description: Display shipping methods in modal
  Version: 0.6
  Author: NIslam
  Author URI: https://wordpress.org/woocommerce-shipping-modal/
  License: GPLv2 or later
  Text Domain: woocommerce-shipping-modal

 * @package Woocommerce Shipping Modal
 *
 */
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  Copyright 2005-2015 Endix Bangladesh Ltd.
 */

if (!defined('ABSPATH')) {
    echo 'Gotcha!!! Dummy.';
    exit; // Exit if accessed directly
}

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('WOOCOMMERCE_SHIPPING_METHOD_MODAL_VERSION', '0.8');
define('WOOCOMMERCE_SHIPPING_METHOD_MODAL_WP_VERSION', '4.0');
define('WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_URL', plugin_dir_url(__FILE__));
define('WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once( WOOCOMMERCE_SHIPPING_METHOD_MODAL_PLUGIN_DIR . 'src/WoocomModal.php' );

$GLOBALS['wc_shipping_modal'] = $WoocomModal = new WoocomModal();

register_activation_hook(__FILE__, array($WoocomModal, 'plugin_activation'));
register_deactivation_hook(__FILE__, array($WoocomModal, 'plugin_deactivation'));
