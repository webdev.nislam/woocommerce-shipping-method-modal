/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(function ($) {

    $(document).ready(function () {

        $(document).on('click', '.set-shipping', function () {
            $('.modal').modal({keyboard: true});
        });

        $(document).on('keydown', function (e) {
            if (27 == e.keyCode) {
                $('.modal').modal('hide');
            }
        });

        if ($('#show_on_page_load').length > 0) {
            var show_on_page_load = $('#show_on_page_load').attr('attr-show_on_page_load'), post_code = $('#global_shipping_methods_post_code').val().trim();

//            if ((show_on_page_load == 1) && (Cookies.get('chosen_shipping_methods') == undefined)) {
//                $('.modal').modal({keyboard: true});
//            }

            if ((show_on_page_load == 1)) {
                $('.modal').modal({keyboard: true});
            }
            
            if(post_code != '') {
                $('#wrapper_global_shipping_methods_post_code').show();
            }
        }

        if ($('#hide_country_list').length > 0) {
            var hide_country_list = $('#hide_country_list').attr('attr-hide_country_list');
            if (hide_country_list == 1) {
                $('#country_dropdown').hide();
            }
        }

        if ($('#select2-shipping-zones').length > 0) {
            $('#select2-shipping-zones').select2({
                selectOnClose: true,
                width: 'resolve',
            });
        }

        $('#select2-shipping-zones').on('select2:select', function (e) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: ajax_shipping_method_object.ajaxurl,
                data: {zone_code: $(this).val(), security: ajax_shipping_method_object.ajax_get_shipping_method_nonce, action: 'get_shipping_method_by_zone'},
                success: function (response) {
                    $('#global_shipping_methods').html('');
                    $('#global_shipping_methods').html(response.html);
                }
            });
        });

        $(document).on('click', '.shipping_method', function () {
            if ($(this).parent('li').find('label').text().trim() != 'Collection') {
                $('#shipping_collection_message').hide();
            } else {

                if ($('#shipping_collection_message').length == 0) {

                    var notice_for_collection = '<div id="shipping_collection_message" class="alert alert-danger" style="display: block;">Your chosen shipping method is <strong>Collection</strong>. This means you\'ll have to collect your order directly from the restaurant.</div>';

                    $('.woocommerce-billing-fields > h3').after(notice_for_collection);

                } else {
                    $('#shipping_collection_message').show();
                }
            }
        });

        $(document).on('change', '#global_shipping_methods input:radio[name="shipping_method"]', function () {
            var global_selected_shipping_method_label = $(this).parent().find('span').text().trim();
            $('#global_selected_shipping_method_label').val(global_selected_shipping_method_label);
            $('#shipping_method_error').hide();

            if (global_selected_shipping_method_label == 'Delivery') {
                $('#wrapper_global_shipping_methods_post_code').show();
            } else {
                $('#wrapper_global_shipping_methods_post_code').hide();
            }
        });

        $(document).on('click', '.set-global-shipping-method', function () {
            var shipping_method = $('.shipping_method_radio:checked').val(),
                    shipping_method_label = $('#global_selected_shipping_method_label').val(),
                    shipping_method_selected_postcode = $('#global_shipping_methods_post_code').val(),
                    shipping_method_selected_country = $('#select2-shipping-zones').val();

            if (shipping_method == undefined) {
                $('#shipping_method_error').show();
                return false;
            } else {
                $('#shipping_method_error').hide();
            }

            if (shipping_method_label == 'Delivery' && (shipping_method_selected_postcode == undefined || shipping_method_selected_postcode.trim() == '')) {
                $('#shipping_method_error').text('Invalid postcode.');
                $('#shipping_method_error').show();
                return false;
            } else {
                $('#shipping_method_error').text('');
                $('#shipping_method_error').hide();
            }

            var payload = {shipping_method_selected_country: shipping_method_selected_country, shipping_method_selected_postcode: shipping_method_selected_postcode, shipping_method_label: shipping_method_label, zone_code: $('#select2-shipping-zones').val(), shipping_method: shipping_method, security: ajax_shipping_method_object.ajax_nonce, action: 'set_global_shipping_method'};

            if (shipping_method != undefined) {
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: ajax_shipping_method_object.ajaxurl,
                    data: payload,
                    success: function (response) {
                        if (response.success) {
                            $('.modal').modal('hide');
                        } else {
                            $('#shipping_method_error').text(response.messages);
                            $('#shipping_method_error').show();
                        }

                    },
                });
            }


        });

    });
});
